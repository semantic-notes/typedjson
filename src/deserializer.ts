import { AbstractDeSerializer } from './abstract-de-serializer';
import { DeserializationConfigError, DeserializationError, DeserializationTypeError } from './exceptions';
import { ISerializerSettings } from './typed-json';
import { ElementType } from './utils';

export class Deserializer<T> extends AbstractDeSerializer {

  public constructor(json: any, type: ElementType<T>, settings: ISerializerSettings = {}, depth: number = 0) {
    super(
      json,
      settings,
      depth,
      type,
      type.name,
      type.prototype,
      {
        config: DeserializationConfigError,
        normal: DeserializationError,
        type:   DeserializationTypeError,
      },
    );
  }

  public deserialize(): T {
    return this.processObject();
  }

  public convertDate(value: string, pk: string): Date {
    const ticks = Date.parse(value);

    if (isNaN(ticks) || ticks < 1) {
      throw new DeserializationError(`Member '${this.className}.${pk}' is not type of DateString else '${value}'`,
                                     this.className,
                                     this.depth,
                                     pk,
      );
    }

    return new Date(ticks);

  }

  protected createNewAbstractDeSerializer(
    value: any,
    type: ElementType<any>,
    settings: ISerializerSettings,
    depth: number): AbstractDeSerializer {
    return new (this.constructor as any)(value, type, settings, depth);
  }

}
