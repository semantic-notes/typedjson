﻿import { Deserializer } from './deserializer';
import { TypedJSONError } from './exceptions';
import { Serializer } from './serializer';
import { ElementType } from './utils';

export interface ISerializerSettings {
  /**
   * Property key to recognize as type-hints. Default is "__type".
   * @deprecated type hints are manged by reflect-metadata
   */
  typeHintPropertyKey?: string;

  /**
   * When set, enable emitting and recognizing type-hints. Default is true
   * @deprecated type hints are manged by reflect-metadata
   */
  enableTypeHints?: boolean;

  /**
   * Maximum number of objects allowed when deserializing from JSON. Default is no limit.
   */
  maxObjects?: number;

  /**
   * Maximum object depth while serializing. Default is 25.
   */
  maxDepth?: number;

  omitEmptyValues?: boolean;

  /**
   * A function that transforms the JSON after serializing. Called recursively for every object.
   */
  replacer?: (key: string, value: any) => any;

  /**
   * A function that transforms the JSON before deserializing. Called recursively for every object.
   */
  reviver?: (key: any, value: any) => any;
}

export abstract class TypedJSON {

  public static config(settings: ISerializerSettings) {
    TypedJSON.configSettings = Object.assign({}, TypedJSON.configSettings, settings);
  }

  public static deserialize<T>(json: Object, type: ElementType<T>, settings: ISerializerSettings = {}): T {
    settings = Object.assign({}, TypedJSON.configSettings, settings);
    return new Deserializer<T>(json, type, settings).deserialize();
  }

  public static deserializeArray<T>(
    json: Object[],
    type: ElementType<T>,
    settings: ISerializerSettings = {},
  ): T[] {
    settings = Object.assign({}, TypedJSON.configSettings, settings);
    if (!Array.isArray(json)) {
      throw new TypedJSONError('JSON string is not an array');
    }
    return json.map((item: any) => TypedJSON.deserialize(item, type, settings));
  }

  public static parse<T>(str: string, type: ElementType<T>, settings: ISerializerSettings = {}): T {
    settings = Object.assign({}, TypedJSON.configSettings, settings);
    return TypedJSON.deserialize<T>(JSON.parse(str, settings.reviver), type, settings);
  }

  public static parseArray<T>(str: string, type: ElementType<T>, settings: ISerializerSettings = {}) {
    settings = Object.assign({}, TypedJSON.configSettings, settings);
    return TypedJSON.deserializeArray(JSON.parse(str, settings.reviver), type, settings);
  }

  public static serialize<T = any>(value: any, settings: ISerializerSettings = {}): T {
    settings = Object.assign({}, TypedJSON.configSettings, settings);
    return new Serializer(value, settings).serialize();
  }

  public static serializeArray<T = any>(values: any[], settings: ISerializerSettings = {}): T[] {
    settings = Object.assign({}, TypedJSON.configSettings, settings);
    if (!Array.isArray(values)) {
      throw new TypedJSONError('value is not an array');
    }
    return values.map((value: any) => TypedJSON.serialize(value, settings));
  }

  public static stringify(value: any, settings: ISerializerSettings = {}): string {
    settings = Object.assign({}, TypedJSON.configSettings, settings);
    return JSON.stringify(TypedJSON.serialize(value, settings), settings.replacer);
  }

  public static stringifyArray(value: any[], settings: ISerializerSettings = {}): string {
    return JSON.stringify(TypedJSON.serializeArray(value, settings), settings.replacer);
  }

  private static configSettings: ISerializerSettings = {};
}

