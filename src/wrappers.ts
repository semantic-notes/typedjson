import { chain, isNumber } from 'lodash';

import { Deserializer } from './deserializer';
import { JsonObject } from './json-object';
import { IJsonMemberOptions } from './member-options';
import { Serializer } from './serializer';
import { ISerializerSettings, TypedJSON } from './typed-json';
import { ElementType } from './utils';

@JsonObject()
export abstract class ITJsonObject {}

@JsonObject()
export class Any {}

@JsonObject()
export class TJsObject {}

export function getConfigSettings(): ISerializerSettings {
  return (TypedJSON as any).configSettings;
}

export function tDeserialize<T>(
  json: Object, type: ElementType<T>, settings: ISerializerSettings = {} ): T
{
  settings = Object.assign({}, getConfigSettings(), settings);
  const deserializer = new TDeserializer<T>(json, type, settings);
  return deserializer.deserialize();
}

export function tSerialize<T>(obj: T, settings: ISerializerSettings = {} ): T {
  settings = Object.assign({}, getConfigSettings(), settings);
  const serializer = new TSerializer<T>(obj, settings);
  return serializer.serialize();
}

export class TDeserializer<T extends ITJsonObject> extends Deserializer<T> {
  constructor(
    json: any,
    type: ElementType<T>,
    settings: ISerializerSettings = {},
    depth: number = 0
  ) { super(json, type, settings, depth); }

  public convertDate(value: string, pk: string): Date {
    const ticks = isNumber(value) ? value : Date.parse(value);
    return new Date(ticks);
  }

  public processProperty(value: any, propertyKey: string, options: IJsonMemberOptions) {
    return super.processProperty(value, propertyKey, options);
  }

  public transform(value: any, propertyKey: string, elementType: ElementType | undefined) {
    switch (elementType) {
      case Any: {
        return value;
      }
      case TJsObject: {
        console.warn(`For prop: ${propertyKey} are used unhandled TJsObject!!!`);
        return value;
      }
      case Map: {
        return chain(value)
          .reduce((res, val, key) => {
            res.set(key, val);
            return res;
          }, new Map())
          .value();
      }
      default: {
        return super.transform(value, propertyKey, elementType);
      }
    }
  }
}

export class TSerializer<T extends ITJsonObject> extends Serializer<T> {
  constructor(
    obj: T,
    settings: ISerializerSettings = {},
    depth: number = 0
  ) { super(obj, settings, depth); }

  protected transform(value: any, propertyKey: string, elementType: ElementType | undefined) {
    switch (elementType) {
      case Any: {
        return value;
      }
      case TJsObject: {
        console.warn(`For prop: ${propertyKey} are used unhandled TJsObject!!!`);
        return value;
      }
      case Map: {
        const ret = {};
        value.forEach((val, key) => {
          ret[key] = val;
        });
        return ret;
      }
      default: {
        return super.transform(value, propertyKey, elementType);
      }
    }
  }
}
