import { DEFAULT_SERIALIZING_DEPTH, TYPED_JSON } from './config';
import { ConfigError, NormalError, TypeError } from './exceptions';
import { getAllJsonMembers } from './json-member';
import { IJsonMemberOptions } from './member-options';
import { ISerializerSettings } from './typed-json';
import { ElementType, getReadableTypeName, isPrimitiveType, JsonMemberType } from './utils';

export interface IErrorTypes {
  normal: NormalError;
  type: TypeError;
  config: ConfigError;
}

export abstract class AbstractDeSerializer {

  public readonly jsonMembers: JsonMemberType;

  protected constructor(
    public readonly source: any,
    public settings: ISerializerSettings,
    public readonly depth: number,
    public readonly targetType: ElementType,
    public readonly className: string,
    public readonly prototype: any,
    protected readonly ErrorTypes: IErrorTypes,
  ) {
    if (this.source === null || this.source === undefined) {
      throw new this.ErrorTypes.normal(`Source value should not be null or undefined. this.source = ${this.source}`,
                                       this.className,
                                       this.depth,
      );
    }
    if (typeof this.source !== 'object') {
      throw new this.ErrorTypes.normal(`Source value should be an object instated found '${getReadableTypeName(this.source)}'`,
                                       this.className,
                                       this.depth,
      );
    }
    if (!this.prototype) {
      throw new this.ErrorTypes.config('FATAL: Prototype is not defined', this.className, this.depth);
    }
    this.jsonMembers = getAllJsonMembers(this.prototype);
  }

  protected abstract createNewAbstractDeSerializer(
    value: any,
    type: ElementType,
    settings: ISerializerSettings,
    depth: number,
  ): AbstractDeSerializer;

  protected abstract convertDate(value: any, pk: string): any;

  protected processObject() {

    if (this.depth >= (this.settings.maxDepth || DEFAULT_SERIALIZING_DEPTH)) {
      throw new this.ErrorTypes.normal(`Max serializing depth is excised by serializing '${this.className}'`,
                                       this.className,
                                       this.depth,
      );
    }

    if (this.targetType !== Object && !this.isTypedJSONInstance()) {
      throw new this.ErrorTypes.config(`Source is not a TypedJSON Instance. Insure that the '@JsonObject()' decorator is added '${this.className}'`,
                                       this.className,
                                       this.depth,
      );
    }

    if (!this.jsonMembers) {
      throw new this.ErrorTypes.config(`Source is not a TypedJSON Instance. Insure that the all '@JsonMember()' decorator is added '${this.className}'`,
                                       this.className,
                                       this.depth,
      );
    }

    const target: any = new this.targetType();

    for (const pk of Object.keys(this.jsonMembers)) {

      const options: IJsonMemberOptions = this.jsonMembers[pk];

      if (!options.name) {
        throw new this.ErrorTypes.config(`Name of member '${this.className}.${pk}' is not defined`,
                                         this.className,
                                         this.depth,
                                         pk,
        );
      }

      let value: any;
      let targetKey: string;
      if (this.targetType === Object) {
        value     = this.source[pk];
        targetKey = options.name;
      } else {
        value     = this.source[options.name];
        targetKey = pk;
      }

      if (value === undefined) {
        if (!this.settings.omitEmptyValues) {
          target[targetKey] = this.processProperty(value, pk, options);
        }
      } else {
        target[targetKey] = this.processProperty(value, pk, options);
      }

    }

    return target;
  }

  protected isTypedJSONInstance(): boolean {
    return Reflect.hasMetadata(TYPED_JSON, this.prototype);
  }

  protected transform(value: any, propertyKey: string, elementType: ElementType | undefined) {

    let target: any | null = null;

    if (elementType) {

      switch (elementType) {

        case Number:
          if (typeof value !== 'number') {
            throw new this.ErrorTypes.type(this.className,
                                           this.depth,
                                           propertyKey,
                                           'Number',
                                           getReadableTypeName(value),
            );
          }
          target = value;
          break;

        case String:
          if (typeof value !== 'string') {
            throw new this.ErrorTypes.type(this.className,
                                           this.depth,
                                           propertyKey,
                                           'String',
                                           getReadableTypeName(value),
            );
          }
          target = value;
          break;

        case Boolean:
          if (typeof value !== 'boolean') {
            throw new this.ErrorTypes.type(this.className,
                                           this.depth,
                                           propertyKey,
                                           'Boolean',
                                           getReadableTypeName(value),
            );
          }
          target = value;
          break;

        case Date:
          target = this.convertDate(value, propertyKey);
          break;

        default:
          if (typeof value !== 'object') {
            throw new this.ErrorTypes.type(this.className,
                                           this.depth,
                                           propertyKey,
                                           elementType.toString(),
                                           getReadableTypeName(value),
            );
          }
          target = this.createNewAbstractDeSerializer(value, elementType, this.settings, this.depth + 1)
            .processObject();
          break;

      }

    } else {
      throw new this.ErrorTypes.config(`Type of member '${this.className}.${propertyKey}' is not defined`,
                                       this.className,
                                       this.depth,
                                       propertyKey,
      );
    }

    return target;

  }

  protected processProperty(value: any, propertyKey: string, options: IJsonMemberOptions) {

    let targetProperty;

    if (!options) {
      throw new this.ErrorTypes.config(`Options of member '${this.className}.${propertyKey}' are not defined`,
                                       this.className,
                                       this.depth,
                                       propertyKey,
      );
    }

    if (!options.type && !options.elements) {
      throw new this.ErrorTypes.config(`Type of member '${this.className}.${propertyKey}' is not defined`,
                                       this.className,
                                       this.depth,
                                       propertyKey,
      );
    }

    if (options.isRequired) {
      if (value === null || value === undefined) {
        throw new this.ErrorTypes.normal(`Member '${this.className}.${propertyKey}' is required`,
                                         this.className,
                                         this.depth,
                                         propertyKey,
        );
      }
    } else {
      if (value === undefined || value === null) {
        if (options.elements) {
          return [];
        } else {
          return null;
        }
      }
    }

    // region type checking and assigning

    if (options.elements) {

      targetProperty = this.array(value, propertyKey, options);

    } else {

      if (options.toObjectMap) {
        targetProperty = options.toObjectMap(value);
      } else {
        targetProperty = this.transform(value, propertyKey, options.type);
      }

    }

    // endregion

    return targetProperty;

  }

  protected simple(value: any, propertyKey: string, elementType: ElementType): any {
    if (isPrimitiveType(elementType)) {
      return this.processProperty(value, propertyKey + '[]', { type: elementType });
    } else {
      return this
        .createNewAbstractDeSerializer(value, elementType, this.settings, this.depth + 1)
        .processObject();
    }
  }

  protected simpleArray(values: any[], propertyKey: string, elementType: ElementType): any[] {
    return values.map((value: any) => {
      return this.simple(value, propertyKey + '[]', elementType);
    });
  }

  protected nestedArray(values: any[], propertyKey: string, options: IJsonMemberOptions): any[] {
    const elements: IJsonMemberOptions = options.elements as any;

    return values.map((value: any) => {

      if (!elements) {
        throw new this.ErrorTypes.config(`Type of member '${this.className}.${propertyKey}' is not defined`,
                                         this.className,
                                         this.depth,
                                         propertyKey,
        );
      }

      if (Array.isArray(value)) {
        return value.map((v: any) => this.simple(v, propertyKey + '[]', elements as any));
      } else {
        throw new this.ErrorTypes.normal(`Member '${this.className}.${propertyKey}' should be a nested array`,
                                         this.className,
                                         this.depth,
                                         propertyKey,
        );
      }

    });
  }

  protected array(values: any[], propertyKey: string, options: IJsonMemberOptions): any[] {
    if (!Array.isArray(values)) {
      throw new this.ErrorTypes.type(this.className, this.depth, propertyKey, 'Array', getReadableTypeName(values));
    }

    if (typeof options.elements === 'object' && typeof options.elements.type === 'function') {
      options.elements = options.elements.type;
    }

    if (typeof options.elements === 'object') {

      return this.nestedArray(values, propertyKey, options.elements);

    } else if (typeof options.elements === 'function') {

      return this.simpleArray(values, propertyKey, options.elements);

    } else {
      throw new this.ErrorTypes.config(`Type of member '${this.className}.${propertyKey}' is not defined`,
                                       this.className,
                                       this.depth,
                                       propertyKey,
      );
    }
  }

}
