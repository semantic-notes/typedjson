export class TypedJSONError extends Error {
  constructor(msg: string) {
    super(msg);
    Error.captureStackTrace(this, TypedJSONError);
  }
}

export type NormalError = new (
  msg: string,
  className: string,
  depth: number,
  propertyKey?: string,
) => SerializationError | DeserializationError;
export type TypeError = new (
  className: string,
  depth: number,
  propertyKey: string,
  targetType: string,
  sourceType: string,
) => SerializationTypeError | DeserializationTypeError;
export type ConfigError = new (
  msg: string,
  className: string,
  depth: number,
  propertyKey?: string,
) => SerializationConfigError | DeserializationConfigError;

export class SerializationError extends TypedJSONError {
  constructor(msg: string, className: string, depth: number, propertyKey?: string) {
    super(msg + ' -|- ' + JSON.stringify({ className, depth, propertyKey }));
    Error.captureStackTrace(this, SerializationError);
  }
}

export class DeserializationError extends TypedJSONError {
  constructor(msg: string, className: string, depth: number, propertyKey?: string) {
    super(msg + ' -|- ' + JSON.stringify({ className, depth, propertyKey }));
    Error.captureStackTrace(this, DeserializationError);
  }
}

export interface ISerializationTypeError {
  new(name: string, targetType: string, sourceType: string): SerializationError;
}

export class SerializationTypeError extends SerializationError {
  constructor(className: string, depth: number, propertyKey: string, targetType: string, sourceType: string) {
    super(`Member '${className}.${propertyKey}' is not type of ${targetType} else '${sourceType}'`,
          className,
          depth,
          propertyKey,
    );
    Error.captureStackTrace(this, SerializationTypeError);
  }
}

export class DeserializationTypeError extends DeserializationError {
  constructor(className: string, depth: number, propertyKey: string, targetType: string, sourceType: string) {
    super(`Member '${className}.${propertyKey}' is not type of ${targetType} else '${sourceType}'`,
          className,
          depth,
          propertyKey,
    );
    Error.captureStackTrace(this, SerializationTypeError);
  }
}

export class SerializationConfigError extends SerializationError {
  constructor(msg: string, className: string, depth: number, propertyKey?: string) {
    super(msg, className, depth, propertyKey);
    Error.captureStackTrace(this, SerializationConfigError);
  }
}

export class DeserializationConfigError extends DeserializationError {
  constructor(msg: string, className: string, depth: number, propertyKey?: string) {
    super(msg, className, depth, propertyKey);
    Error.captureStackTrace(this, DeserializationConfigError);
  }
}
