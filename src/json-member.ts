import { DESIGN_TYPE, JSON_MEMBER } from './config';
import { TypedJSONError } from './exceptions';
import { IJsonMemberOptions } from './member-options';
import { JsonMemberType } from './utils';

export function JsonMember(options: IJsonMemberOptions = {}) {

  if (options.elementType) {
    options.elements = { type: options.elementType };
    console.warn(`'IJsonMemberOptions.elementType' is deprecated instead use 'IJsonMemberOptions.elements'`);
  }

  if (options.order) {
    console.warn(`'IJsonMemberOptions.order' is currently not supported`);
  }

  if (options.emitDefaultValue) {
    console.warn(`'IJsonMemberOptions.emitDefaultValue' is currently not supported`);
  }

  if (options.refersAbstractType) {
    console.warn(`'IJsonMemberOptions.refersAbstractType' is currently not supported`);
  }

  // tslint:disable:space-before-function-paren
  return function (target: any, propertyKey: string) {
    Reflect.defineMetadata(`${JSON_MEMBER}-${propertyKey}`, Object.assign({}, {
      isRequired: false, name: propertyKey, type: Reflect.getMetadata(DESIGN_TYPE, target, propertyKey),
    }, options), target);
  };
}

export function getAllJsonMembers(prototype: any): JsonMemberType {
  if (!prototype) {
    throw new TypedJSONError('FATAL: Prototype is not defined');
  }
  const keys: string[]              = Reflect.getMetadataKeys(prototype)
    .filter((key: string) => key.includes(JSON_MEMBER));
  const jsonMembers: JsonMemberType = {};
  for (const key of keys) {
    jsonMembers[key.substr(JSON_MEMBER.length + 1, key.length - JSON_MEMBER.length - 1)] =
      Reflect.getMetadata(key, prototype);
  }
  return jsonMembers;
}
