export const TYPED_JSON                = 'typed-json';
export const JSON_OBJECT               = 'json-object';
export const JSON_MEMBER               = 'json-member';
export const DESIGN_TYPE               = 'design:type';
export const DEFAULT_SERIALIZING_DEPTH = 25;
