import { JSON_OBJECT, TYPED_JSON } from './config';
import { IJsonObjectOptions } from './object-options';

export function JsonObject(options: IJsonObjectOptions = {}) {

  if (options.name) {
    console.warn(`'IJsonObjectOptions.name' is currently not supported`);
  }

  if (options.initializer) {
    console.warn(`'IJsonObjectOptions.initializer' is currently not supported`);
  }

  if (options.serializer) {
    console.warn(`'IJsonObjectOptions.serializer' is currently not supported`);
  }

  if (options.knownTypes) {
    console.warn(`'IJsonObjectOptions.knownTypes' is currently not supported`);
  }

  // tslint:disable:space-before-function-paren
  return function (target: any) {
    Reflect.defineMetadata(TYPED_JSON, true, target.prototype);
    Reflect.defineMetadata(JSON_OBJECT, options, target.prototype);
  };
}
