import { IJsonMemberOptions } from './member-options';

export function extractPrototype(instance: any): any {
  if (!instance.constructor) {
    throw new Error('This obj is not a instance of a class');
  }

  if (!instance.constructor.prototype) {
    throw new Error('Could not find the class prototype');
  }

  return instance.constructor.prototype;

}

export function extractClassName(instance: any): string {
  if (!instance.constructor) {
    throw new Error('This obj is not a instance of a class');
  }

  if (!instance.constructor.name) {
    throw new Error('Could not find the class name');
  }

  return instance.constructor.name;
}

export type JsonMemberType = { [key: string]: IJsonMemberOptions };

export type ElementType<T = any> = new (...args: any[]) => T;

export function isPrimitive(value: any): boolean {
  const typeOf = typeof value;
  return typeOf === 'string' || typeOf === 'number' || typeOf === 'boolean' || value instanceof Date;
}

export function isPrimitiveType(type: ElementType): boolean {
  return type === Number || type === String || type === Boolean || type === Date;
}

export function getReadableName(value: any) {

  if (typeof value === 'object') {
    return getReadableTypeName(value);
  } else {
    return value;
  }

}

export function getReadableTypeName(value: any): string {
  if (value === null) {
    return 'null';
  }
  if (value === undefined) {
    return 'undefined';
  }
  if (typeof value === 'object') {

    if (value.constructor) {
      return value.constructor.name;
    } else {
      return 'object';
    }

  } else {
    return typeof value;
  }
}
