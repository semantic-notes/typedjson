import 'reflect-metadata';
import { AbstractDeSerializer } from './abstract-de-serializer';
import { SerializationConfigError, SerializationError, SerializationTypeError } from './exceptions';
import { ISerializerSettings } from './typed-json';
import { extractClassName, extractPrototype, getReadableTypeName, ElementType } from './utils';
import { IJsonMemberOptions } from './member-options';

export class Serializer<JSON extends Object = any> extends AbstractDeSerializer {

  constructor(instance: any, settings: ISerializerSettings = {}, depth: number = 0) {
    super(
      instance,
      settings,
      depth,
      Object,
      extractClassName(instance),
      extractPrototype(instance),
      {
        config: SerializationConfigError,
        normal: SerializationError,
        type:   SerializationTypeError,
      },
    );
  }

  public serialize(): any {
    return this.processObject();
  }

  public processProperty(value: any, propertyKey: string, options: IJsonMemberOptions) {

    let targetProperty;

    if (!options) {
      throw new this.ErrorTypes.config(`Options of member '${this.className}.${propertyKey}' are not defined`,
                                       this.className,
                                       this.depth,
                                       propertyKey,
      );
    }

    if (!options.type && !options.elements) {
      throw new this.ErrorTypes.config(`Type of member '${this.className}.${propertyKey}' is not defined`,
                                       this.className,
                                       this.depth,
                                       propertyKey,
      );
    }

    if (options.isRequired) {
      if (value === null || value === undefined) {
        throw new this.ErrorTypes.normal(`Member '${this.className}.${propertyKey}' is required`,
                                         this.className,
                                         this.depth,
                                         propertyKey,
        );
      }
    } else {
      if (value === undefined || value === null) {
        if (options.elements) {
          return [];
        } else {
          return null;
        }
      }
    }

    // region type checking and assigning

    if (options.elements) {

      targetProperty = this.array(value, propertyKey, options);

    } else {
      if (options.toJsonMap) {
        targetProperty = options.toJsonMap(value);
      } else {
        targetProperty = this.transform(value, propertyKey, options.type);
      }
    }

    // endregion

    return targetProperty;

  }

  public convertDate(value: Date, pk: string): number {
    if (value instanceof Date) {
      return value.getTime();
    } else {
      throw new SerializationError(
        `Member '${this.className}.${pk}' is not type of Date else '${getReadableTypeName(value)}'`, 
        this.className, this.depth, pk);
    }
  }

  protected createNewAbstractDeSerializer(
    value: any,
    type: ElementType<any>,
    settings: ISerializerSettings,
    depth: number): AbstractDeSerializer {
    return new (this.constructor as any)(value, settings, depth);
  }

}
