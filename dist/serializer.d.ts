import 'reflect-metadata';
import { AbstractDeSerializer } from './abstract-de-serializer';
import { ISerializerSettings } from './typed-json';
import { ElementType } from './utils';
import { IJsonMemberOptions } from './member-options';
export declare class Serializer<JSON extends Object = any> extends AbstractDeSerializer {
    constructor(instance: any, settings?: ISerializerSettings, depth?: number);
    serialize(): any;
    processProperty(value: any, propertyKey: string, options: IJsonMemberOptions): any;
    convertDate(value: Date, pk: string): number;
    protected createNewAbstractDeSerializer(value: any, type: ElementType<any>, settings: ISerializerSettings, depth: number): AbstractDeSerializer;
}
