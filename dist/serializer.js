"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
const abstract_de_serializer_1 = require("./abstract-de-serializer");
const exceptions_1 = require("./exceptions");
const utils_1 = require("./utils");
class Serializer extends abstract_de_serializer_1.AbstractDeSerializer {
    constructor(instance, settings = {}, depth = 0) {
        super(instance, settings, depth, Object, utils_1.extractClassName(instance), utils_1.extractPrototype(instance), {
            config: exceptions_1.SerializationConfigError,
            normal: exceptions_1.SerializationError,
            type: exceptions_1.SerializationTypeError,
        });
    }
    serialize() {
        return this.processObject();
    }
    processProperty(value, propertyKey, options) {
        let targetProperty;
        if (!options) {
            throw new this.ErrorTypes.config(`Options of member '${this.className}.${propertyKey}' are not defined`, this.className, this.depth, propertyKey);
        }
        if (!options.type && !options.elements) {
            throw new this.ErrorTypes.config(`Type of member '${this.className}.${propertyKey}' is not defined`, this.className, this.depth, propertyKey);
        }
        if (options.isRequired) {
            if (value === null || value === undefined) {
                throw new this.ErrorTypes.normal(`Member '${this.className}.${propertyKey}' is required`, this.className, this.depth, propertyKey);
            }
        }
        else {
            if (value === undefined || value === null) {
                if (options.elements) {
                    return [];
                }
                else {
                    return null;
                }
            }
        }
        // region type checking and assigning
        if (options.elements) {
            targetProperty = this.array(value, propertyKey, options);
        }
        else {
            if (options.toJsonMap) {
                targetProperty = options.toJsonMap(value);
            }
            else {
                targetProperty = this.transform(value, propertyKey, options.type);
            }
        }
        // endregion
        return targetProperty;
    }
    convertDate(value, pk) {
        if (value instanceof Date) {
            return value.getTime();
        }
        else {
            throw new exceptions_1.SerializationError(`Member '${this.className}.${pk}' is not type of Date else '${utils_1.getReadableTypeName(value)}'`, this.className, this.depth, pk);
        }
    }
    createNewAbstractDeSerializer(value, type, settings, depth) {
        return new this.constructor(value, settings, depth);
    }
}
exports.Serializer = Serializer;
