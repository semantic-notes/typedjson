"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const abstract_de_serializer_1 = require("./abstract-de-serializer");
const exceptions_1 = require("./exceptions");
class Deserializer extends abstract_de_serializer_1.AbstractDeSerializer {
    constructor(json, type, settings = {}, depth = 0) {
        super(json, settings, depth, type, type.name, type.prototype, {
            config: exceptions_1.DeserializationConfigError,
            normal: exceptions_1.DeserializationError,
            type: exceptions_1.DeserializationTypeError,
        });
    }
    deserialize() {
        return this.processObject();
    }
    convertDate(value, pk) {
        const ticks = Date.parse(value);
        if (isNaN(ticks) || ticks < 1) {
            throw new exceptions_1.DeserializationError(`Member '${this.className}.${pk}' is not type of DateString else '${value}'`, this.className, this.depth, pk);
        }
        return new Date(ticks);
    }
    createNewAbstractDeSerializer(value, type, settings, depth) {
        return new this.constructor(value, type, settings, depth);
    }
}
exports.Deserializer = Deserializer;
