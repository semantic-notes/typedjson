"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const deserializer_1 = require("./deserializer");
const exceptions_1 = require("./exceptions");
const serializer_1 = require("./serializer");
class TypedJSON {
    static config(settings) {
        TypedJSON.configSettings = Object.assign({}, TypedJSON.configSettings, settings);
    }
    static deserialize(json, type, settings = {}) {
        settings = Object.assign({}, TypedJSON.configSettings, settings);
        return new deserializer_1.Deserializer(json, type, settings).deserialize();
    }
    static deserializeArray(json, type, settings = {}) {
        settings = Object.assign({}, TypedJSON.configSettings, settings);
        if (!Array.isArray(json)) {
            throw new exceptions_1.TypedJSONError('JSON string is not an array');
        }
        return json.map((item) => TypedJSON.deserialize(item, type, settings));
    }
    static parse(str, type, settings = {}) {
        settings = Object.assign({}, TypedJSON.configSettings, settings);
        return TypedJSON.deserialize(JSON.parse(str, settings.reviver), type, settings);
    }
    static parseArray(str, type, settings = {}) {
        settings = Object.assign({}, TypedJSON.configSettings, settings);
        return TypedJSON.deserializeArray(JSON.parse(str, settings.reviver), type, settings);
    }
    static serialize(value, settings = {}) {
        settings = Object.assign({}, TypedJSON.configSettings, settings);
        return new serializer_1.Serializer(value, settings).serialize();
    }
    static serializeArray(values, settings = {}) {
        settings = Object.assign({}, TypedJSON.configSettings, settings);
        if (!Array.isArray(values)) {
            throw new exceptions_1.TypedJSONError('value is not an array');
        }
        return values.map((value) => TypedJSON.serialize(value, settings));
    }
    static stringify(value, settings = {}) {
        settings = Object.assign({}, TypedJSON.configSettings, settings);
        return JSON.stringify(TypedJSON.serialize(value, settings), settings.replacer);
    }
    static stringifyArray(value, settings = {}) {
        return JSON.stringify(TypedJSON.serializeArray(value, settings), settings.replacer);
    }
}
TypedJSON.configSettings = {};
exports.TypedJSON = TypedJSON;
