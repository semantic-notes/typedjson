import { ElementType } from './utils';
export interface IJsonMemberOptions<T = any> {
    /**
     * Sets the member name as it appears in the serialized JSON. Default value is determined from property key.
     */
    name?: string;
    /**
     * Sets the json member type. Optional if reflect metadata is available.
     */
    type?: ElementType<T>;
    /**
     * When the json member is an array, sets the type of array elements. Required for arrays.
     * @deprecated
     */
    elementType?: ElementType<T>;
    /**
     * When the json member is an array, sets the type of array elements. Required for arrays.
     */
    elements?: IJsonMemberOptions<any> | ElementType<T>;
    /**
     * When set, indicates that the member must be present when deserializing a JSON string.
     */
    isRequired?: boolean;
    /**
     * Sets the serialization and deserialization order of the json member.
     * @deprecated
     */
    order?: number;
    /**
     * When set, a default value is emitted when an uninitialized member is serialized.
     * @deprecated
     */
    emitDefaultValue?: boolean;
    /**
     * When set, type-hint is mandatory when deserializing. Set for properties with interface or abstract
     * types/element-types.
     * @deprecated
     */
    refersAbstractType?: boolean;
    /**
     * When set, is used for convert value when deserializing
     * types/element-types.
     */
    toObjectMap?: (raw: any) => T;
    /**
     * When set, is used for convert value when serializing
     * types/element-types.
     */
    toJsonMap?: (obj: T) => any;
}
