export declare const TYPED_JSON = "typed-json";
export declare const JSON_OBJECT = "json-object";
export declare const JSON_MEMBER = "json-member";
export declare const DESIGN_TYPE = "design:type";
export declare const DEFAULT_SERIALIZING_DEPTH = 25;
