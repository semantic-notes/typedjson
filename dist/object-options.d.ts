export interface IJsonObjectOptions<T = any> {
    /**
     * Name of the object as it appears in the serialized JSON.
     * @deprecated currently not supported
     */
    name?: string;
    /**
     * An array of known types to recognize when encountering type-hints.
     * @deprecated currently not supported
     */
    knownTypes?: Array<{
        new (): any;
    }>;
    /**
     * A custom serializer function transforming an instace to a JSON object.
     * @deprecated currently not supported
     */
    serializer?: (object: T) => any;
    /**
     * A custom deserializer function transforming a JSON object to an instace.
     * @deprecated currently not supported
     */
    initializer?: (json: any) => T;
}
