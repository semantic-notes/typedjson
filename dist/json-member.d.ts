import { IJsonMemberOptions } from './member-options';
import { JsonMemberType } from './utils';
export declare function JsonMember(options?: IJsonMemberOptions): (target: any, propertyKey: string) => void;
export declare function getAllJsonMembers(prototype: any): JsonMemberType;
