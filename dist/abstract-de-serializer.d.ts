import { ConfigError, NormalError, TypeError } from './exceptions';
import { IJsonMemberOptions } from './member-options';
import { ISerializerSettings } from './typed-json';
import { ElementType, JsonMemberType } from './utils';
export interface IErrorTypes {
    normal: NormalError;
    type: TypeError;
    config: ConfigError;
}
export declare abstract class AbstractDeSerializer {
    readonly source: any;
    settings: ISerializerSettings;
    readonly depth: number;
    readonly targetType: ElementType;
    readonly className: string;
    readonly prototype: any;
    protected readonly ErrorTypes: IErrorTypes;
    readonly jsonMembers: JsonMemberType;
    protected constructor(source: any, settings: ISerializerSettings, depth: number, targetType: ElementType, className: string, prototype: any, ErrorTypes: IErrorTypes);
    protected abstract createNewAbstractDeSerializer(value: any, type: ElementType, settings: ISerializerSettings, depth: number): AbstractDeSerializer;
    protected abstract convertDate(value: any, pk: string): any;
    protected processObject(): any;
    protected isTypedJSONInstance(): boolean;
    protected transform(value: any, propertyKey: string, elementType: ElementType | undefined): any;
    protected processProperty(value: any, propertyKey: string, options: IJsonMemberOptions): any;
    protected simple(value: any, propertyKey: string, elementType: ElementType): any;
    protected simpleArray(values: any[], propertyKey: string, elementType: ElementType): any[];
    protected nestedArray(values: any[], propertyKey: string, options: IJsonMemberOptions): any[];
    protected array(values: any[], propertyKey: string, options: IJsonMemberOptions): any[];
}
