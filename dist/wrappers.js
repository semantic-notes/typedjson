"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = require("lodash");
const deserializer_1 = require("./deserializer");
const json_object_1 = require("./json-object");
const serializer_1 = require("./serializer");
const typed_json_1 = require("./typed-json");
let ITJsonObject = class ITJsonObject {
};
ITJsonObject = __decorate([
    json_object_1.JsonObject()
], ITJsonObject);
exports.ITJsonObject = ITJsonObject;
let Any = class Any {
};
Any = __decorate([
    json_object_1.JsonObject()
], Any);
exports.Any = Any;
let TJsObject = class TJsObject {
};
TJsObject = __decorate([
    json_object_1.JsonObject()
], TJsObject);
exports.TJsObject = TJsObject;
function getConfigSettings() {
    return typed_json_1.TypedJSON.configSettings;
}
exports.getConfigSettings = getConfigSettings;
function tDeserialize(json, type, settings = {}) {
    settings = Object.assign({}, getConfigSettings(), settings);
    const deserializer = new TDeserializer(json, type, settings);
    return deserializer.deserialize();
}
exports.tDeserialize = tDeserialize;
function tSerialize(obj, settings = {}) {
    settings = Object.assign({}, getConfigSettings(), settings);
    const serializer = new TSerializer(obj, settings);
    return serializer.serialize();
}
exports.tSerialize = tSerialize;
class TDeserializer extends deserializer_1.Deserializer {
    constructor(json, type, settings = {}, depth = 0) { super(json, type, settings, depth); }
    convertDate(value, pk) {
        const ticks = lodash_1.isNumber(value) ? value : Date.parse(value);
        return new Date(ticks);
    }
    processProperty(value, propertyKey, options) {
        return super.processProperty(value, propertyKey, options);
    }
    transform(value, propertyKey, elementType) {
        switch (elementType) {
            case Any: {
                return value;
            }
            case TJsObject: {
                console.warn(`For prop: ${propertyKey} are used unhandled TJsObject!!!`);
                return value;
            }
            case Map: {
                return lodash_1.chain(value)
                    .reduce((res, val, key) => {
                    res.set(key, val);
                    return res;
                }, new Map())
                    .value();
            }
            default: {
                return super.transform(value, propertyKey, elementType);
            }
        }
    }
}
exports.TDeserializer = TDeserializer;
class TSerializer extends serializer_1.Serializer {
    constructor(obj, settings = {}, depth = 0) { super(obj, settings, depth); }
    transform(value, propertyKey, elementType) {
        switch (elementType) {
            case Any: {
                return value;
            }
            case TJsObject: {
                console.warn(`For prop: ${propertyKey} are used unhandled TJsObject!!!`);
                return value;
            }
            case Map: {
                const ret = {};
                value.forEach((val, key) => {
                    ret[key] = val;
                });
                return ret;
            }
            default: {
                return super.transform(value, propertyKey, elementType);
            }
        }
    }
}
exports.TSerializer = TSerializer;
