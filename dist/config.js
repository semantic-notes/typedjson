"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TYPED_JSON = 'typed-json';
exports.JSON_OBJECT = 'json-object';
exports.JSON_MEMBER = 'json-member';
exports.DESIGN_TYPE = 'design:type';
exports.DEFAULT_SERIALIZING_DEPTH = 25;
