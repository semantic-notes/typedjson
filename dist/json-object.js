"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
function JsonObject(options = {}) {
    if (options.name) {
        console.warn(`'IJsonObjectOptions.name' is currently not supported`);
    }
    if (options.initializer) {
        console.warn(`'IJsonObjectOptions.initializer' is currently not supported`);
    }
    if (options.serializer) {
        console.warn(`'IJsonObjectOptions.serializer' is currently not supported`);
    }
    if (options.knownTypes) {
        console.warn(`'IJsonObjectOptions.knownTypes' is currently not supported`);
    }
    // tslint:disable:space-before-function-paren
    return function (target) {
        Reflect.defineMetadata(config_1.TYPED_JSON, true, target.prototype);
        Reflect.defineMetadata(config_1.JSON_OBJECT, options, target.prototype);
    };
}
exports.JsonObject = JsonObject;
