"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TypedJSONError extends Error {
    constructor(msg) {
        super(msg);
        Error.captureStackTrace(this, TypedJSONError);
    }
}
exports.TypedJSONError = TypedJSONError;
class SerializationError extends TypedJSONError {
    constructor(msg, className, depth, propertyKey) {
        super(msg + ' -|- ' + JSON.stringify({ className, depth, propertyKey }));
        Error.captureStackTrace(this, SerializationError);
    }
}
exports.SerializationError = SerializationError;
class DeserializationError extends TypedJSONError {
    constructor(msg, className, depth, propertyKey) {
        super(msg + ' -|- ' + JSON.stringify({ className, depth, propertyKey }));
        Error.captureStackTrace(this, DeserializationError);
    }
}
exports.DeserializationError = DeserializationError;
class SerializationTypeError extends SerializationError {
    constructor(className, depth, propertyKey, targetType, sourceType) {
        super(`Member '${className}.${propertyKey}' is not type of ${targetType} else '${sourceType}'`, className, depth, propertyKey);
        Error.captureStackTrace(this, SerializationTypeError);
    }
}
exports.SerializationTypeError = SerializationTypeError;
class DeserializationTypeError extends DeserializationError {
    constructor(className, depth, propertyKey, targetType, sourceType) {
        super(`Member '${className}.${propertyKey}' is not type of ${targetType} else '${sourceType}'`, className, depth, propertyKey);
        Error.captureStackTrace(this, SerializationTypeError);
    }
}
exports.DeserializationTypeError = DeserializationTypeError;
class SerializationConfigError extends SerializationError {
    constructor(msg, className, depth, propertyKey) {
        super(msg, className, depth, propertyKey);
        Error.captureStackTrace(this, SerializationConfigError);
    }
}
exports.SerializationConfigError = SerializationConfigError;
class DeserializationConfigError extends DeserializationError {
    constructor(msg, className, depth, propertyKey) {
        super(msg, className, depth, propertyKey);
        Error.captureStackTrace(this, DeserializationConfigError);
    }
}
exports.DeserializationConfigError = DeserializationConfigError;
