"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
const json_member_1 = require("./json-member");
const utils_1 = require("./utils");
class AbstractDeSerializer {
    constructor(source, settings, depth, targetType, className, prototype, ErrorTypes) {
        this.source = source;
        this.settings = settings;
        this.depth = depth;
        this.targetType = targetType;
        this.className = className;
        this.prototype = prototype;
        this.ErrorTypes = ErrorTypes;
        if (this.source === null || this.source === undefined) {
            throw new this.ErrorTypes.normal(`Source value should not be null or undefined. this.source = ${this.source}`, this.className, this.depth);
        }
        if (typeof this.source !== 'object') {
            throw new this.ErrorTypes.normal(`Source value should be an object instated found '${utils_1.getReadableTypeName(this.source)}'`, this.className, this.depth);
        }
        if (!this.prototype) {
            throw new this.ErrorTypes.config('FATAL: Prototype is not defined', this.className, this.depth);
        }
        this.jsonMembers = json_member_1.getAllJsonMembers(this.prototype);
    }
    processObject() {
        if (this.depth >= (this.settings.maxDepth || config_1.DEFAULT_SERIALIZING_DEPTH)) {
            throw new this.ErrorTypes.normal(`Max serializing depth is excised by serializing '${this.className}'`, this.className, this.depth);
        }
        if (this.targetType !== Object && !this.isTypedJSONInstance()) {
            throw new this.ErrorTypes.config(`Source is not a TypedJSON Instance. Insure that the '@JsonObject()' decorator is added '${this.className}'`, this.className, this.depth);
        }
        if (!this.jsonMembers) {
            throw new this.ErrorTypes.config(`Source is not a TypedJSON Instance. Insure that the all '@JsonMember()' decorator is added '${this.className}'`, this.className, this.depth);
        }
        const target = new this.targetType();
        for (const pk of Object.keys(this.jsonMembers)) {
            const options = this.jsonMembers[pk];
            if (!options.name) {
                throw new this.ErrorTypes.config(`Name of member '${this.className}.${pk}' is not defined`, this.className, this.depth, pk);
            }
            let value;
            let targetKey;
            if (this.targetType === Object) {
                value = this.source[pk];
                targetKey = options.name;
            }
            else {
                value = this.source[options.name];
                targetKey = pk;
            }
            if (value === undefined) {
                if (!this.settings.omitEmptyValues) {
                    target[targetKey] = this.processProperty(value, pk, options);
                }
            }
            else {
                target[targetKey] = this.processProperty(value, pk, options);
            }
        }
        return target;
    }
    isTypedJSONInstance() {
        return Reflect.hasMetadata(config_1.TYPED_JSON, this.prototype);
    }
    transform(value, propertyKey, elementType) {
        let target = null;
        if (elementType) {
            switch (elementType) {
                case Number:
                    if (typeof value !== 'number') {
                        throw new this.ErrorTypes.type(this.className, this.depth, propertyKey, 'Number', utils_1.getReadableTypeName(value));
                    }
                    target = value;
                    break;
                case String:
                    if (typeof value !== 'string') {
                        throw new this.ErrorTypes.type(this.className, this.depth, propertyKey, 'String', utils_1.getReadableTypeName(value));
                    }
                    target = value;
                    break;
                case Boolean:
                    if (typeof value !== 'boolean') {
                        throw new this.ErrorTypes.type(this.className, this.depth, propertyKey, 'Boolean', utils_1.getReadableTypeName(value));
                    }
                    target = value;
                    break;
                case Date:
                    target = this.convertDate(value, propertyKey);
                    break;
                default:
                    if (typeof value !== 'object') {
                        throw new this.ErrorTypes.type(this.className, this.depth, propertyKey, elementType.toString(), utils_1.getReadableTypeName(value));
                    }
                    target = this.createNewAbstractDeSerializer(value, elementType, this.settings, this.depth + 1)
                        .processObject();
                    break;
            }
        }
        else {
            throw new this.ErrorTypes.config(`Type of member '${this.className}.${propertyKey}' is not defined`, this.className, this.depth, propertyKey);
        }
        return target;
    }
    processProperty(value, propertyKey, options) {
        let targetProperty;
        if (!options) {
            throw new this.ErrorTypes.config(`Options of member '${this.className}.${propertyKey}' are not defined`, this.className, this.depth, propertyKey);
        }
        if (!options.type && !options.elements) {
            throw new this.ErrorTypes.config(`Type of member '${this.className}.${propertyKey}' is not defined`, this.className, this.depth, propertyKey);
        }
        if (options.isRequired) {
            if (value === null || value === undefined) {
                throw new this.ErrorTypes.normal(`Member '${this.className}.${propertyKey}' is required`, this.className, this.depth, propertyKey);
            }
        }
        else {
            if (value === undefined || value === null) {
                if (options.elements) {
                    return [];
                }
                else {
                    return null;
                }
            }
        }
        // region type checking and assigning
        if (options.elements) {
            targetProperty = this.array(value, propertyKey, options);
        }
        else {
            if (options.toObjectMap) {
                targetProperty = options.toObjectMap(value);
            }
            else {
                targetProperty = this.transform(value, propertyKey, options.type);
            }
        }
        // endregion
        return targetProperty;
    }
    simple(value, propertyKey, elementType) {
        if (utils_1.isPrimitiveType(elementType)) {
            return this.processProperty(value, propertyKey + '[]', { type: elementType });
        }
        else {
            return this
                .createNewAbstractDeSerializer(value, elementType, this.settings, this.depth + 1)
                .processObject();
        }
    }
    simpleArray(values, propertyKey, elementType) {
        return values.map((value) => {
            return this.simple(value, propertyKey + '[]', elementType);
        });
    }
    nestedArray(values, propertyKey, options) {
        const elements = options.elements;
        return values.map((value) => {
            if (!elements) {
                throw new this.ErrorTypes.config(`Type of member '${this.className}.${propertyKey}' is not defined`, this.className, this.depth, propertyKey);
            }
            if (Array.isArray(value)) {
                return value.map((v) => this.simple(v, propertyKey + '[]', elements));
            }
            else {
                throw new this.ErrorTypes.normal(`Member '${this.className}.${propertyKey}' should be a nested array`, this.className, this.depth, propertyKey);
            }
        });
    }
    array(values, propertyKey, options) {
        if (!Array.isArray(values)) {
            throw new this.ErrorTypes.type(this.className, this.depth, propertyKey, 'Array', utils_1.getReadableTypeName(values));
        }
        if (typeof options.elements === 'object' && typeof options.elements.type === 'function') {
            options.elements = options.elements.type;
        }
        if (typeof options.elements === 'object') {
            return this.nestedArray(values, propertyKey, options.elements);
        }
        else if (typeof options.elements === 'function') {
            return this.simpleArray(values, propertyKey, options.elements);
        }
        else {
            throw new this.ErrorTypes.config(`Type of member '${this.className}.${propertyKey}' is not defined`, this.className, this.depth, propertyKey);
        }
    }
}
exports.AbstractDeSerializer = AbstractDeSerializer;
