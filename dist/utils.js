"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function extractPrototype(instance) {
    if (!instance.constructor) {
        throw new Error('This obj is not a instance of a class');
    }
    if (!instance.constructor.prototype) {
        throw new Error('Could not find the class prototype');
    }
    return instance.constructor.prototype;
}
exports.extractPrototype = extractPrototype;
function extractClassName(instance) {
    if (!instance.constructor) {
        throw new Error('This obj is not a instance of a class');
    }
    if (!instance.constructor.name) {
        throw new Error('Could not find the class name');
    }
    return instance.constructor.name;
}
exports.extractClassName = extractClassName;
function isPrimitive(value) {
    const typeOf = typeof value;
    return typeOf === 'string' || typeOf === 'number' || typeOf === 'boolean' || value instanceof Date;
}
exports.isPrimitive = isPrimitive;
function isPrimitiveType(type) {
    return type === Number || type === String || type === Boolean || type === Date;
}
exports.isPrimitiveType = isPrimitiveType;
function getReadableName(value) {
    if (typeof value === 'object') {
        return getReadableTypeName(value);
    }
    else {
        return value;
    }
}
exports.getReadableName = getReadableName;
function getReadableTypeName(value) {
    if (value === null) {
        return 'null';
    }
    if (value === undefined) {
        return 'undefined';
    }
    if (typeof value === 'object') {
        if (value.constructor) {
            return value.constructor.name;
        }
        else {
            return 'object';
        }
    }
    else {
        return typeof value;
    }
}
exports.getReadableTypeName = getReadableTypeName;
