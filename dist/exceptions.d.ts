export declare class TypedJSONError extends Error {
    constructor(msg: string);
}
export declare type NormalError = new (msg: string, className: string, depth: number, propertyKey?: string) => SerializationError | DeserializationError;
export declare type TypeError = new (className: string, depth: number, propertyKey: string, targetType: string, sourceType: string) => SerializationTypeError | DeserializationTypeError;
export declare type ConfigError = new (msg: string, className: string, depth: number, propertyKey?: string) => SerializationConfigError | DeserializationConfigError;
export declare class SerializationError extends TypedJSONError {
    constructor(msg: string, className: string, depth: number, propertyKey?: string);
}
export declare class DeserializationError extends TypedJSONError {
    constructor(msg: string, className: string, depth: number, propertyKey?: string);
}
export interface ISerializationTypeError {
    new (name: string, targetType: string, sourceType: string): SerializationError;
}
export declare class SerializationTypeError extends SerializationError {
    constructor(className: string, depth: number, propertyKey: string, targetType: string, sourceType: string);
}
export declare class DeserializationTypeError extends DeserializationError {
    constructor(className: string, depth: number, propertyKey: string, targetType: string, sourceType: string);
}
export declare class SerializationConfigError extends SerializationError {
    constructor(msg: string, className: string, depth: number, propertyKey?: string);
}
export declare class DeserializationConfigError extends DeserializationError {
    constructor(msg: string, className: string, depth: number, propertyKey?: string);
}
