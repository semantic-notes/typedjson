"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = require("./config");
const exceptions_1 = require("./exceptions");
function JsonMember(options = {}) {
    if (options.elementType) {
        options.elements = { type: options.elementType };
        console.warn(`'IJsonMemberOptions.elementType' is deprecated instead use 'IJsonMemberOptions.elements'`);
    }
    if (options.order) {
        console.warn(`'IJsonMemberOptions.order' is currently not supported`);
    }
    if (options.emitDefaultValue) {
        console.warn(`'IJsonMemberOptions.emitDefaultValue' is currently not supported`);
    }
    if (options.refersAbstractType) {
        console.warn(`'IJsonMemberOptions.refersAbstractType' is currently not supported`);
    }
    // tslint:disable:space-before-function-paren
    return function (target, propertyKey) {
        Reflect.defineMetadata(`${config_1.JSON_MEMBER}-${propertyKey}`, Object.assign({}, {
            isRequired: false, name: propertyKey, type: Reflect.getMetadata(config_1.DESIGN_TYPE, target, propertyKey),
        }, options), target);
    };
}
exports.JsonMember = JsonMember;
function getAllJsonMembers(prototype) {
    if (!prototype) {
        throw new exceptions_1.TypedJSONError('FATAL: Prototype is not defined');
    }
    const keys = Reflect.getMetadataKeys(prototype)
        .filter((key) => key.includes(config_1.JSON_MEMBER));
    const jsonMembers = {};
    for (const key of keys) {
        jsonMembers[key.substr(config_1.JSON_MEMBER.length + 1, key.length - config_1.JSON_MEMBER.length - 1)] =
            Reflect.getMetadata(key, prototype);
    }
    return jsonMembers;
}
exports.getAllJsonMembers = getAllJsonMembers;
