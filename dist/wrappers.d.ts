import { Deserializer } from './deserializer';
import { IJsonMemberOptions } from './member-options';
import { Serializer } from './serializer';
import { ISerializerSettings } from './typed-json';
import { ElementType } from './utils';
export declare abstract class ITJsonObject {
}
export declare class Any {
}
export declare class TJsObject {
}
export declare function getConfigSettings(): ISerializerSettings;
export declare function tDeserialize<T>(json: Object, type: ElementType<T>, settings?: ISerializerSettings): T;
export declare function tSerialize<T>(obj: T, settings?: ISerializerSettings): T;
export declare class TDeserializer<T extends ITJsonObject> extends Deserializer<T> {
    constructor(json: any, type: ElementType<T>, settings?: ISerializerSettings, depth?: number);
    convertDate(value: string, pk: string): Date;
    processProperty(value: any, propertyKey: string, options: IJsonMemberOptions): any;
    transform(value: any, propertyKey: string, elementType: ElementType | undefined): any;
}
export declare class TSerializer<T extends ITJsonObject> extends Serializer<T> {
    constructor(obj: T, settings?: ISerializerSettings, depth?: number);
    protected transform(value: any, propertyKey: string, elementType: ElementType | undefined): any;
}
