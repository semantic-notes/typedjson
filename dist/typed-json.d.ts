import { ElementType } from './utils';
export interface ISerializerSettings {
    /**
     * Property key to recognize as type-hints. Default is "__type".
     * @deprecated type hints are manged by reflect-metadata
     */
    typeHintPropertyKey?: string;
    /**
     * When set, enable emitting and recognizing type-hints. Default is true
     * @deprecated type hints are manged by reflect-metadata
     */
    enableTypeHints?: boolean;
    /**
     * Maximum number of objects allowed when deserializing from JSON. Default is no limit.
     */
    maxObjects?: number;
    /**
     * Maximum object depth while serializing. Default is 25.
     */
    maxDepth?: number;
    omitEmptyValues?: boolean;
    /**
     * A function that transforms the JSON after serializing. Called recursively for every object.
     */
    replacer?: (key: string, value: any) => any;
    /**
     * A function that transforms the JSON before deserializing. Called recursively for every object.
     */
    reviver?: (key: any, value: any) => any;
}
export declare abstract class TypedJSON {
    static config(settings: ISerializerSettings): void;
    static deserialize<T>(json: Object, type: ElementType<T>, settings?: ISerializerSettings): T;
    static deserializeArray<T>(json: Object[], type: ElementType<T>, settings?: ISerializerSettings): T[];
    static parse<T>(str: string, type: ElementType<T>, settings?: ISerializerSettings): T;
    static parseArray<T>(str: string, type: ElementType<T>, settings?: ISerializerSettings): T[];
    static serialize<T = any>(value: any, settings?: ISerializerSettings): T;
    static serializeArray<T = any>(values: any[], settings?: ISerializerSettings): T[];
    static stringify(value: any, settings?: ISerializerSettings): string;
    static stringifyArray(value: any[], settings?: ISerializerSettings): string;
    private static configSettings;
}
