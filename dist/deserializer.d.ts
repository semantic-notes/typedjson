import { AbstractDeSerializer } from './abstract-de-serializer';
import { ISerializerSettings } from './typed-json';
import { ElementType } from './utils';
export declare class Deserializer<T> extends AbstractDeSerializer {
    constructor(json: any, type: ElementType<T>, settings?: ISerializerSettings, depth?: number);
    deserialize(): T;
    convertDate(value: string, pk: string): Date;
    protected createNewAbstractDeSerializer(value: any, type: ElementType<any>, settings: ISerializerSettings, depth: number): AbstractDeSerializer;
}
