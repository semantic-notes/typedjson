import { IJsonMemberOptions } from './member-options';
export declare function extractPrototype(instance: any): any;
export declare function extractClassName(instance: any): string;
export declare type JsonMemberType = {
    [key: string]: IJsonMemberOptions;
};
export declare type ElementType<T = any> = new (...args: any[]) => T;
export declare function isPrimitive(value: any): boolean;
export declare function isPrimitiveType(type: ElementType): boolean;
export declare function getReadableName(value: any): any;
export declare function getReadableTypeName(value: any): string;
