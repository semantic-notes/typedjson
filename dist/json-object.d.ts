import { IJsonObjectOptions } from './object-options';
export declare function JsonObject(options?: IJsonObjectOptions): (target: any) => void;
